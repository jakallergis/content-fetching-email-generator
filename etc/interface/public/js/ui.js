// Disable pressing enter key on forms
$("form").keypress(
    function(event){
     if (event.which == '13') {
        event.preventDefault();
      }
});

$( document ).ready(function(){

	// Detect Prducts input field changes in order to display the submit button or not
	$("#products").on("change keyup paste click", function(){
	    	var pInputs = document.getElementById("products").getElementsByTagName("input");
	    	var checker = 0;
	    	for (var i = 0; i < pInputs.length; i++) {
	    		if (pInputs[i].value === ''){
	    			checker++
	    		}
	    	}
	    	if (checker === 0) {
	    		document.getElementById("submitProducts").classList.remove("hidden");
	    	} else {
	    		document.getElementById("submitProducts").className += " hidden";
	    	}
	});

	// Detect Categories input field changes in order to display the submit button or not
	$("#categories").on("change keyup paste click", function(){
	    	var pInputs = document.getElementById("categories").getElementsByTagName("input");
	    	var checker = 0;
	    	for (var i = 0; i < pInputs.length; i++) {
	    		if (pInputs[i].value === ''){
	    			checker++
	    		}
	    	}
	    	if (checker === 0) {
	    		document.getElementById("submitCategories").classList.remove("hidden");
	    	} else {
	    		document.getElementById("submitCategories").className += " hidden";
	    	}
	});
	
	// Setting the initial products and categories amount
	var initialProduct = document.getElementById("products").innerHTML;
	var initialCategory = document.getElementById("categories").innerHTML;
	// Adding more Product inputs on #addProduct click.
	document.getElementById("addProduct").addEventListener('click', function() {
		     var products = document.getElementById("products");
		     var productsCounter = products.getElementsByTagName("input").length + 1;
		     var row = document.createElement("div")
		    	var inputDiv = document.createElement("div");
		    	var input = document.createElement("input");
		    	var label = document.createElement("label");

		    	row.setAttribute("class", "row")
		    	inputDiv.setAttribute("class", "input-field col s6")	

		    	input.id = "product-"+productsCounter;
		    	input.type = "number";

		    	label.setAttribute("for", "product-"+productsCounter);
		    	label.innerHTML= `Product ${productsCounter} ID`;

		    	products.appendChild(inputDiv);
		    	inputDiv.appendChild(input);
		    	inputDiv.appendChild(label);
		    	document.getElementById("resetProducts").classList.remove("hidden");
		    	document.getElementById("submitProducts").className += " hidden";
	});

	// Adding more Category inputs on #addProduct click.
	document.getElementById("addCategory").addEventListener('click', function() {
		     var categories = document.getElementById("categories");
		     var categoriesCounter = categories.getElementsByTagName("input").length + 1;
		     var counterDiv = document.createElement("div");
		     var counterP = document.createElement("p");
		     var counter = categoriesCounter+". "
		    	var inputDiv = document.createElement("div");
		    	var input = document.createElement("input");
		    	var label = document.createElement("label");

		    	counterDiv.setAttribute("class", "input-field col s1")
		    	inputDiv.setAttribute("class", "input-field col s2")	

		    	input.id = "category-"+categoriesCounter;
		    	input.type = "number";

		    	label.setAttribute("for", "category-"+categoriesCounter);
		    	label.innerHTML= "ID";

		    	categories.appendChild(counterDiv);
		    	counterDiv.appendChild(counterP);
		    	counterP.innerHTML = counter;
		    	categories.appendChild(inputDiv);
		    	inputDiv.appendChild(input);
		    	inputDiv.appendChild(label);
		    	document.getElementById("resetCategories").classList.remove("hidden");
		    	document.getElementById("submitCategories").className += " hidden";
	});
	
	// Resseting the products amount to the initial amount and hiding the reset button.
	document.getElementById("resetProducts").addEventListener('click', function() {
		document.getElementById("products").innerHTML = initialProduct;
		document.getElementById("resetProducts").className += " hidden";
	});

	// Resseting the Categories amount to the initial amount and hiding the reset button.
	document.getElementById("resetCategories").addEventListener('click', function() {
		document.getElementById("categories").innerHTML = initialCategory;
		document.getElementById("resetCategories").className += " hidden";
	});

	// Needed function to open the side menu on mobile devices.
	$(".button-collapse").sideNav();


})