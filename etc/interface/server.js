var fs = require('fs');
var fetch = require('../functions/fetcher');
var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app).listen(3000);
var io = require("socket.io")(server);

app.use(express.static("./etc/interface/public"));

io.on("connection", function(socket) {

	socket.on("products", function(data) {
		fetch.generateProducts(data.url, data.path, data.productIds);
	});

	socket.on("categories", function(data) {
		fetch.generateCategories(data.url, data.path, data.categoryIds);
	});

});

console.log("Starting Socket App - http://localhost:3000");