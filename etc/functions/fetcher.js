var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');

function pRequest(url, link, i) {
    request(link, function(error, response, html){
        if(!error){
            var $ = cheerio.load(html); {

                var pName, pImage, pLink, pPrice;

                $('.product-info h1').filter(function(){
                    var data = $(this);
                    pName = data.text().split("     ",1).toString();
                })

                pLink = link;

                $('.cm-previewer img').filter(function(){
                    var data = $(this);
                    pImage = url+data.attr().src;
                })

                $('.price .nowrap').first().filter(function(){
                    var data = $(this);
                    pPrice = data.text();
                })

                $('.price .nowrap').last().filter(function(){
                    var data = $(this);
                    pPrice = pPrice+data.text();
                })

                fs.writeFile(`src/partials/tmp/pName${i+1}.html`, pName);
                console.log(`pName${i+1}.html`);
                fs.writeFile(`src/partials/tmp/pLink${i+1}.html`, pLink);
                console.log(`pLink${i+1}.html`);
                fs.writeFile(`src/partials/tmp/pImage${i+1}.html`, pImage);
                console.log(`pImage${i+1}.html`);
                fs.writeFile(`src/partials/tmp/pPrice${i+1}.html`, pPrice);
                console.log(`pPrice${i+1}.html`);
            }
        } else {
            console.log("there was an error with the connection");
        }
    })
}

function cRequest(url, cID){
    request(url, function(error, response, html){
        if(!error){
            var $ = cheerio.load(html);
            for (var i = 0; i < cID.length; i++) {
                var cName, cImage, cLink;            
                $(cID[i] + ' .category_name_small').filter(function(){
                    var data = $(this);
                    cName = data.text();            
                })
                $(cID[i]).filter(function(){
                    var data = $(this);
                    cLink = url+"/"+data.children().first().attr().href;
                })
                $(cID[i] + ' img').filter(function(){
                    var data = $(this);
                    cImage = url+"/"+data.attr().src;          
                })
                var catObj = `<a href="${cLink}"><center><img src="${cImage}"></center><p class="text-center">${cName}</p></a>`;
                fs.writeFile(`src/partials/tmp/cat${i+1}.html`, catObj);
                console.log(`cat${i+1}.html`);
            }
        } else {
            console.log("There was an error with the connection");
        }
    })
}

exports.generateProducts = function(url, path, productIds){
    for (var i = 0; i < productIds.length; i++) {
        var link = url+path+productIds[i];
        pRequest(url, link, i);
    }
}

exports.generateCategories = function (url, path, categoryIds) {
    console.log("I reached exports.generateCategories");
    cRequest(url, categoryIds);
}