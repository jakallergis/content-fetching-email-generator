# [Content Fetching Email Generator](#)

*by John Kallergis*

This is a tool that will generate the HTML/CSS for ready-to-send emails, right after fetching the content from the web.
It runs on Node.js using the Zurb's Foundation for Emails framework to generate the emails, Express and sockets to serve the interface to provide the url, paths, products and categories, and Request and Cheerio to fetch the content.

>As this is still a work in progress, I am using the newsletter for [Stella's Art Collection](http:/stellasart-collection.gr) as a sample project. It will be built based on this project but at the end the tool should be applicable to any email project.


#### Installation

`npm install` will install everything needed.


#### The Interface

`npm start` to start the web interface on port 3000. You can then connect to it, and fill the needed fields in order to fetch the content.

**Fetch and Include the needed content**

The fetching process is done by fetcher.js inside the functions folder.

**Products**

Once inside the web interface, insert the url of the site you want to fetch the content from, the path used by products (ex: /index.php?dispatch=products.view&product_id=), and the Product Ids. If you want more products you can press the red "plus" button and then the red button. Once you have filled all of the product Ids the fetch button will appear. Clicking on it will fetch everything inside their corresponding files.

**Categories**

Again, once inside the web interface, insert the url of the site you want to fetch the content from, the path to the page where all the categories are listed, and the Category Ids. If you want more categories you can press the red "plus" button and then the yellow button. Once you have filled all of the product Ids the fetch button will appear. Clicking on it will fetch everything inside their corresponding files.

* ***NOTE #1:** if the page were the categories are listed is the homepage, you can leave the categories path empty.*
* ***NOTE #2:** for now, and since I'm using stellasart-collection.gr as a project, all the categories are listed in the homepage, and since I want the category images as well, i cant use the category url or the category id if i want to fetch a category into my design. Instead i provide the class name used by CS-Cart to proioritise the listing. (ex. the element with the class .10 is the first category listed inside the homepage.) This will be changed upon finishing the project*

**example:**

Lets say you fill the form using the following data:
Field | Input
--- | --- |
Site url | http://stellasart-collection.gr |
Products Path | /index.php?dispatch=products.view&product_id= |
Product 1 ID | 6346 |
Product 2 ID | 6348 |
Product 3 ID | 6347 |
Product 4 ID | 6345 |
Categories Path | *empty* |
1. ID | 10 |
2. ID | 550 |
3. ID | 40 |
4. ID | 270 |
5. ID | 80 |
6. ID | 90 |

This will generate HTML files to be used as handlebars:

Handlebar | Filename | Example |
--- | ---  | --- |
Product Name | pNameX.html | `{{>pName3}}` |
Product Link | pLinkX.html | `{{>pLink3}}` |
Product Image | pImageX.html | `{{>pImage3}}` |
Product Price | pPriceX.html | `{{>pPrice3}}` |
Category Block | catX.html | `{{>cat6}}` |

>in this example these handlebars will refer to the name,link, image and price of the 3rd product fetched from http://stellasart-collection.gr/index.php?dispatch=products.view&product_id= which is the product with id 6347, and the image, link and name of the 6th category fetched from http://stellasart-collection.gr which is the category with the class .90

Use these handlebars anywhere inside src HTML files.


#### The build

`npm run build` to do a full email inlining process. This will uglify all generated HTML files, and insert the CSS inside them. A new browser window will open showing the email to be sent, with BrowserSync to refresh as you modify contents of the email.

`npm run zip` to generate the HTML files and create a .zip archive for every generated HTML, that contains the HTML file and its assets. These archives are ready to be used with your ESP.


*More info will be provided as the project progresses...*