var socket = io("http://localhost:3000");

// On submitProducts click, get everything and emit them to the server to the corresponding events
document.getElementById("submitProducts").addEventListener("click", function() {
	var url = document.getElementById("url").value;
	var path = document.getElementById("ppath").value;
	var products = document.getElementById("products").getElementsByTagName("input");
	var productIds = [];
	for (var i = 0; i < products.length; i++) {
		productIds.push(products[i].value);
	}
	var data = {
		"url": url,
		"path": path,
		"productIds": productIds
	}
	socket.emit("products", data);
});

// On submitCategories click, get everything and emit them to the server to the corresponding events
document.getElementById("submitCategories").addEventListener("click", function() {
	var url = document.getElementById("url").value;
	var path = document.getElementById("cpath").value;
	var categories = document.getElementById("categories").getElementsByTagName("input");
	var categoryIds = [];
	for (var i = 0; i < categories.length; i++) {
		categoryIds.push("."+categories[i].value);
	}
	var data = {
		"url": url,
		"path": path,
		"categoryIds": categoryIds
	}
	socket.emit("categories", data);
});